'use strict';
let managerSelect = document.getElementById('manager');
let departmentSelect = document.getElementById('depart');
managerSelect.addEventListener('change', function() {
	if (this.selectedIndex > 0) {
		departmentSelect.setAttribute('disabled', 'disabled');
	} else {
		departmentSelect.removeAttribute('disabled');
	}
});
departmentSelect.addEventListener('change', function() {
	if (this.selectedIndex > 0) {
		managerSelect.setAttribute('disabled', 'disabled');
	} else {
		managerSelect.removeAttribute('disabled');
	}
});