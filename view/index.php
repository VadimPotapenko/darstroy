<!DOCTYPE html>
<html>
	<head>
		<title>Отчет по лидам</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="view/styles/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="view/styles/custom.css?1">
	</head>
	<body>

		<!-- фильтр -->
		<form action="index.php" method='POST'>
			<div class="container-fluid bg-light py-3">
				<div class="row">
					<div class="col-3">
						<label for="manager" class="form__label">Менеджер:</label>
						<select name='manager' id='manager' class="form-control">
								<option class='default' value=''>Менеджер не выбран</option>
								<?php foreach ($users[0]['result']['result'] as $user): ?>
									<?php foreach ($user as $u): ?>
										<?php $selecMan = ($manager == $u['ID']) ? 'selected' : ''; ?>
										<option <?=$selecMan;?> value="<?= $u['ID']; ?>"><?= $u['LAST_NAME'].' '.$u['NAME']; ?></option>
									<?php endforeach; ?>
								<?php endforeach; ?>
						</select>
					</div>
					<div class="col-3">
						<label for="depart" class="form__label">Отдел:</label>
						<select name='depart' id='depart' class="form-control">
								<option class='default' value=''>Отдел не выбран</option>
								<?php foreach ($department['result'] as $departs): ?>
									<?php $selecDep = ($depart == $departs['ID']) ? 'selected' : ''; ?>
									<option <?=$selecDep;?> value="<?= $departs['ID']; ?>"><?= $departs['NAME']; ?></option>
								<?php endforeach; ?>
						</select>
					</div>
					<div class="col-3">
						<label for="date" class="form__label">Период:</label>
						<div id="date">
							<div class="container-fluid">
								<div class="row">
									<div class="col-6">
										<input class="form-control" type="date" name='from' value="<?=$_POST['from'];?>">
									</div>
									<div class="col-6">
										<input class="form-control" type="date" name='to' value="<?=$_POST['to'];?>">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-3">
						<input id="send" type="submit" class="btn btn-primary send" name="send">
					</div>
				</div>
			</div>
		</form>


		<!-- таблица -->
		<table class="table my-3">
			<thead>
				<tr>
					<th class="alert-secondary">Менеджер</th>
					<th class="alert-secondary">Лиды</th>
					<th class='alert-secondary'>Качественный<span>/ %</span></th>
					<th class='alert-secondary'>Не качественный<span>/ %</span></th>
					<th class="alert-secondary">Встреча назначена<span>/ %</span></th>
					<th class="alert-secondary">Встреча проведена<span>/ %</span></th>
					<th class="alert-secondary">Сделки<span>/ %</span></th>
				</tr>
			</thead>

			<tfoot>
				<tr>
					<td class="alert-secondary">Итого</td>
					<td class="alert-secondary"><?=$totalArr['lead'] ?: 0;?></td>
					<td class="alert-secondary"><?=$totalArr['quality'] ?: 0;?><span>/ <?=$totalArr['quality_con'] ?: 0;?></span></td>
					<td class="alert-secondary"><?=$totalArr['not_quality'] ?: 0;?><span>/ <?=$totalArr['not_quality_con'] ?: 0;?></span></td>
					<td class="alert-secondary"><?=$totalArr['dateFrom'] ?: 0;?><span>/ <?=$totalArr['dateFrom_con'] ?: 0;?></span></td>
					<td class="alert-secondary"><?=$totalArr['meeting'] ?: 0;?><span>/ <?=$totalArr['meeting_con'] ?: 0;?></span></td>
					<td class="alert-secondary"><?=$totalArr['dateTo'] ?: 0;?><span>/ <?=$totalArr['dateTo_con'] ?: 0;?></span></td>
				</tr>
			</tfoot>

			<tbody>
				<?php if (isset($mainArr)): ?>
					<?php foreach ($mainArr as $person): ?>
						<tr>
							<td><?=$person['manager'];?></td>
							<td><?=$person['lead'] ?: 0;?></td>
							<td><?=$person['quality'] ?: 0;?><span>/ <?=$person['quality_con'] ?: 0;?></span></td>
							<td><?=$person['not_quality'] ?: 0;?><span>/ <?=$person['not_quality_con'] ?: 0;?></span></td>
							<td><?=$person['dateFrom'] ?: 0;?><span>/ <?=$person['dateFrom_con'] ?: 0;?></span></td>
							<td><?=$person['meeting'] ?: 0;?><span>/ <?=$person['meeting_con'] ?: 0;?></span></td>
							<td><?=$person['dateTo'] ?: 0;?><span>/ <?=$person['dateTo_con'] ?: 0;?></span></td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>

		<script src="view/js/main.js?1" defer></script>

	</body>
</html>