<?php
// namespace libs\debugger;
class Debugger {

	/*
	* Функция логгирования
	* @var $data      - данные для записи
	* @var str $path  - путь к фалу
	* @var str $title - 
	*/
	public static function writeToLog($data, $path, $title = 'DEBUG', $bool = true) {
		if ($bool) {
			$log = "\n--------------------\n";
			$log .= date('d.m.Y H:i:s')."\n";
			$log .= $title."\n";
			$log .= print_r($data, 1);
			$log .= "\n--------------------\n";
			file_put_contents($path, $log, FILE_APPEND);
		}
		return true;
	}

	/*
	* Функция вывода данных на экран
	* @var $data - выводимые данные
	*/
	public static function debug($data, $die = false, $dieMessage = 'die') {
		echo '<pre>';
		print_r ($data);
		echo '</pre>';
		if ($die) die('Скрипт принудительно остановлен: '.$dieMessage);
	}

	/*
	* Запись данных в php файл
	* @var $data     - данные для записи в файл
	* @var str $path - путь к файлу
	*/
	public static function saveParams($data, $path) {
		$config = "<?php\n";
		$config .= "\$appsConfig = ".var_export($data, true)."\n";
		$config .= "?>";

		file_put_contents($path, $config);
		return true;
	}
}