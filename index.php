<?php
#========================================== setting ===============================================#
include (__DIR__.'/libs/crest/CRest.php');
include (__DIR__.'/libs/debugger/Debugger.php');
define ('CLIENT', __DIR__.'/libs/crest/settings.json');          // OAuth 2.0
define ('LOG_PATH', __DIR__.'/log/log.txt');                    // куда логируем
define ('LOG_BOOL', false);                                    // управление логированием
define ('DEPARTMENTS', array('57', '58', '59', '60', '150')); // рабочие отделы
define ('IBLOCK', '43');                                     // id списка
#==================================================================================================#
if (isset($_REQUEST['PLACEMENT']) && $_REQUEST['PLACEMENT'] == 'DEFAULT' && !file_exists(CLIENT)) {
	### установка приложения ###
	require (__DIR__.'/libs/crest/install.php');
} else {
	### настройка прав доступа ###
	$accessFilter = DEPARTMENTS;
	$currentUser = CRest::call ('user.current', array());
	$isAdmin = CRest::call ('user.admin', array());
	if (!$isAdmin['result']) {
		if (in_array($currentUser['result']['UF_DEPARTMENT'][0], DEPARTMENTS)) {
			$isHead = CRest::call ('department.get', array('ID' => $currentUser['result']['UF_DEPARTMENT'][0]))['result'][0]['UF_HEAD'];
			if ($currentUser['result']['ID'] == $isHead) $accessFilter = array('UF_DEPARTMENT' => $currentUser['result']['UF_DEPARTMENT'][0]);
			else $accessFilter = array('UF_DEPARTMENT' => $currentUser['result']['UF_DEPARTMENT'][0]);
		} else { require ('message.php'); }
	}

	### подготовка фильтра ###
	$userFilter = array('UF_DEPARTMENT' => $accessFilter, 'ACTIVE' => '1');
	$departmentFilter = array('ID' => $accessFilter);
	### получаем всех пользователей ###
	$usersTotal = CRest::call ('user.get', array('filter' => $userFilter));
	$iteration = intval($usersTotal['total'] / 50) + 1;
	if ($usersTotal['total'] % 50 == 0) $iteration -= 1;
	for ($i = 0; $i < $iteration; $i++) {
		$start = $i * 50;
		$usersData[] = array(
			'method' => 'user.get',
			'params' => array(
				'start' => $start,
				'filter' => $userFilter,
			)
		);
	}
	if (count($usersData) > 50) $usersData = array_chunk($usersData, 50);
	else $usersData = array($usersData);
	for ($i = 0, $s = count($usersData); $i < $s; $i++) {
		$users[] = CRest::callBatch ($usersData[$i]);
	}

	foreach ($users[0]['result']['result'] as $person) {
		foreach ($person as $value) {
			$userView[$value['ID']] = $value['LAST_NAME'].' '.$value['NAME'];
		}
	}

	### сортировка сотрудников в фильтре ###
	$userViewTmp = $userView;
	usort($userViewTmp, function ($a, $b) { if ($a > $b) return 1; });
	foreach ($userViewTmp as $value) {
		foreach ($userView as $k => $v) {
			if ($v == $value) $userShow[$k] = $value;
		}
	}

	### получаем подразделения и источники для таблицы ###
	$department = CRest::call ('department.get', array('filter' => $departmentFilter));
	$source = CRest::call ('crm.status.entity.items', array('entityId' => 'SOURCE'));
	### формируем таблицу согласно фильтру ###
	if (isset($_REQUEST['send'])) {
		### пользовательские поля для фильтра ###
		$listsFilter = array();
		$manager  = $_REQUEST['manager'];           // ответственный
		$depart   = $_REQUEST['depart'];           // депортамент
		$dateFrom = strtotime($_REQUEST['from']); // дата от
		$dateTo   = strtotime($_REQUEST['to']);  // дата до
		$origin   = $_REQUEST['origin'];        // источник

		if (!empty($dateFrom)) $listsFilter['dateFrom'] = $dateFrom;
		if (!empty($dateTo)) $listsFilter['dateTo'] = $dateTo;
		if (!empty($depart)) {
			foreach ($users[0]['result']['result'] as $user) {
				foreach ($user as $u) {
					if (current($u['UF_DEPARTMENT']) == $depart) {
						$listsFilter[] = $u['ID'];
					}
				}
			}
		} elseif (!empty($manager)) {
			$listsFilter[] = $manager;

		} elseif (empty($depart) && empty($manager)) {
			foreach ($users[0]['result']['result'] as $user) {
				foreach ($user as $u) {
					$listsFilter[] = $u['ID'];
				}
			}
		}
		### получаем списки батчем ###
		if (isset($listsFilter)) {
			foreach ($listsFilter as $filt) {
				$listsData[] = array(
					'method' => 'lists.element.get',
					'params' => array(
						'IBLOCK_TYPE_ID' => 'lists',
						'IBLOCK_ID'      => IBLOCK,
						'FILTER'         => array(
							'PROPERTY_131' => $filt,
						)
					)
				);
			}
		}
		$lists = CRest::callBatch ($listsData);

		### формируем массив для вывода данных ###
		foreach ($users[0]['result']['result'] as $userForMain) {
			foreach ($userForMain as $us) {
				foreach ($lists['result']['result'] as $tmp) {
					for ($i = 0, $s = count($tmp); $i < $s; $i++) {
						if ($us['ID'] == current($tmp[$i]['PROPERTY_131'])) {
							if (!empty($dateFrom) && !empty($dateTo)) {
								### в фильтре установлена дата ###
								$mainArr[$us['ID']]['manager'] = $us['LAST_NAME'].' '.$us['NAME'];
								if ((strtotime(current($tmp[$i]['PROPERTY_133'])) >= $dateFrom) && (strtotime(current($tmp[$i]['PROPERTY_133'])) <= $dateTo)) {
									$mainArr[$us['ID']]['lead'] += 1;
									if (current($tmp[$i]['PROPERTY_137']) == '89') $mainArr[$us['ID']]['quality'] += 1;
									elseif (current($tmp[$i]['PROPERTY_137']) == '90') $mainArr[$us['ID']]['not_quality'] += 1;
								}
								if ((strtotime(current($tmp[$i]['PROPERTY_134'])) >= $dateFrom) && (strtotime(current($tmp[$i]['PROPERTY_134'])) <= $dateTo)) {
									$mainArr[$us['ID']]['dateFrom'] += 1;
								}
								if ((strtotime(current($tmp[$i]['PROPERTY_135'])) >= $dateFrom) && (strtotime(current($tmp[$i]['PROPERTY_135'])) <= $dateTo)) {
									$mainArr[$us['ID']]['meeting'] += 1;
								}
								if ((strtotime(current($tmp[$i]['PROPERTY_136'])) >= $dateFrom) && (strtotime(current($tmp[$i]['PROPERTY_136'])) <= $dateTo)) {
									$mainArr[$us['ID']]['dateTo'] += 1;
								}

							} else {
								### в фильтре не установлена дата ###
								$mainArr[$us['ID']]['manager'] = $us['LAST_NAME'].' '.$us['NAME'];
								$mainArr[$us['ID']]['lead'] += !empty(current($tmp[$i]['PROPERTY_133'])) ? 1 : 0;
								$mainArr[$us['ID']]['dateFrom'] += !empty(current($tmp[$i]['PROPERTY_134'])) ? 1 : 0;
								$mainArr[$us['ID']]['meeting'] += !empty(current($tmp[$i]['PROPERTY_135'])) ? 1 : 0;
								$mainArr[$us['ID']]['dateTo'] += !empty(current($tmp[$i]['PROPERTY_136'])) ? 1 : 0;
								if (current($tmp[$i]['PROPERTY_137']) == '89') {
									$mainArr[$us['ID']]['quality'] += 1;
								} elseif (current($tmp[$i]['PROPERTY_137']) == '90') {
									$mainArr[$us['ID']]['not_quality'] += 1;
								}
							}
						}
					}
				}
			}
		}
		### подсчет конверсии ###
		foreach ($mainArr as $k => $v) {
			$totalArr['lead'] += $v['lead'];
			$totalArr['quality'] += $v['quality'];
			$totalArr['not_quality'] += $v['not_quality'];
			$totalArr['dateFrom'] += $v['dateFrom'];
			$totalArr['meeting'] += $v['meeting'];
			$totalArr['dateTo'] += $v['dateTo'];

			$totalArr['quality_con'] = round($totalArr['quality'] / $totalArr['lead'] * 100);
			$totalArr['not_quality_con'] = round($totalArr['not_quality'] / $totalArr['lead'] * 100);
			$totalArr['dateFrom_con'] = round($totalArr['dateFrom'] / $totalArr['quality'] * 100);
			$totalArr['meeting_con'] = round($totalArr['meeting'] / $totalArr['quality'] * 100);
			$totalArr['dateTo_con'] = round($totalArr['dateTo'] / $totalArr['quality'] * 100);

			$mainArr[$k]['quality_con'] = round($v['quality'] / $v['lead'] * 100);
			$mainArr[$k]['not_quality_con'] = round($v['not_quality'] / $v['lead'] * 100);
			$mainArr[$k]['dateFrom_con'] = round($v['dateFrom'] / $v['quality'] * 100);
			$mainArr[$k]['meeting_con'] = round($v['meeting'] / $v['quality'] * 100);
			$mainArr[$k]['dateTo_con'] = round($v['dateTo'] / $v['quality'] * 100);
		}

		### сортировка сотрудников в таблице ###
		$mainArrKeys = array_keys($mainArr);
		usort($mainArr, function($a, $b) {
			if ($a['manager'] > $b['manager']) return 1;
		});
		$mainArr = array_combine($mainArrKeys, $mainArr);
	}
}
require (__DIR__.'/view/index.php');